= Changelog

All notable changes to this project will be documented in this file.

The format is based on https://keepachangelog.com/en/1.0.0/[Keep a Changelog] and this project adheres to https://semver.org/spec/v2.0.0.html[Semantic Versioning].

== [nng-rs Unreleased] ==

=== Added ===

* Sockets can now send messages asynchronously via `Socket::send_async`.
* Sockets can now receive messages asynchronously via `Socket::recv_async`.
* Sockets can now be cloned to have multiple handles to the same underlying NNG socket.

=== Changed ===

* The majority of types are now at the root of the crate rather than a module.
* Asynchronous sending on a context is now via `Context::send` rather than `Aio::send`.
* Asynchronous receiving on a context is now via `Context::recv` rather than `Aio::recv`.

=== Deprecated ===

=== Removed ===

=== Fixed ===

* The `Push0` protocol now actually opens a `Push0` socket (instead of a `Pull0` socket).
* Asynchronous I/O callbacks no longer can enter the same callback closure simultaneously.

=== Security ===

== [nng-sys Unreleased] ==

=== Added ===

=== Changed ===

=== Deprecated ===

=== Removed ===

=== Fixed ===

=== Security ===

//------------------------------------------------------------------------------
// Past Releases
//------------------------------------------------------------------------------

== [nng-rs v0.3.0] - 2018-11-02 ==

First fully usable version.

== [nng-sys v0.1.2] - 2018-11-02 ==

First fully usable version
